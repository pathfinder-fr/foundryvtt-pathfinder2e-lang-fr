#!/bin/bash

set -e

echo "Downloading latest PF2e lang files"
mkdir .ci/ || true
wget -q https://github.com/foundryvtt/pf2e/releases/latest/download/pf2e.zip -O .ci/pf2e.zip
unzip -o -d .ci/ .ci/pf2e.zip # ZIP du système
mv .ci/lang/ ../module/lang
rm -rf .ci/

echo "Done"